import os

from fastapi import FastAPI, Request, Response
from fastapi.templating import Jinja2Templates

from routers.chat import router as chat_router

app = FastAPI()

template_path = os.path.join(os.path.dirname(__file__), "templates")
templates = Jinja2Templates(directory=template_path)


@app.get("/")
async def index(request: Request) -> Response:
    return templates.TemplateResponse("index.html", context={"request": request})


app.include_router(router=chat_router)
