import abc

from fastapi import WebSocket


class WsManager(abc.ABC):
    async def connect(self, websocket: WebSocket) -> None:
        ...

    async def disconnect(self, client_id: str) -> None:
        ...

    async def broadcast(self, data: str) -> None:
        ...

    async def send_personal_data(self, data: str) -> None:
        ...


class ChatManager(WsManager):
    _chats: dict[str, dict[str, WebSocket]] = {}

    def __init__(self, chat_id: str, client_id: str):
        self._chat_id = chat_id
        self._client_id = client_id

        if self._chat_id not in ChatManager._chats:
            ChatManager._chats[self._chat_id] = {}

        self._chat: dict[str, WebSocket] = ChatManager._chats[self._chat_id]

    async def connect(self, websocket: WebSocket) -> None:
        await websocket.accept()

        self._chat[self._client_id] = websocket

    async def disconnect(self, client_id: str) -> None:
        self._chat.pop(client_id)

        if not self._chat:
            ChatManager._chats.pop(self._chat_id)

    async def broadcast(self, data: str) -> None:
        for client_id, ws in self._chat.items():
            if client_id == self._client_id:
                continue

            await ws.send_text(data)

    async def send_personal_data(self, data: str) -> None:
        await self._chat[self._client_id].send_text(data)
