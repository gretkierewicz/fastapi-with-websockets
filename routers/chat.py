import os
from uuid import uuid4

from fastapi import APIRouter, Depends, Request, Response, WebSocket, WebSocketDisconnect
from fastapi.responses import RedirectResponse
from fastapi.templating import Jinja2Templates

from utils import connection

template_path = os.path.join(os.path.dirname(__file__), "templates")
templates = Jinja2Templates(directory=template_path)

router = APIRouter(prefix="/chat")


@router.get("/create")
async def create_chat() -> Response:
    chat_id = uuid4()
    return RedirectResponse(f"/chat/{chat_id}")


@router.get("/{chat_id}")
async def chat_get(request: Request, chat_id: str) -> Response:
    return templates.TemplateResponse(
        "chat.html", context={"request": request, "chat_id": chat_id}
    )


@router.websocket("/{chat_id}/client/{client_id}")
async def websocket_endpoint(
    websocket: WebSocket,
    client_id: str,
    manager: connection.WsManager = Depends(connection.ChatManager)
) -> None:
    await manager.connect(websocket)
    try:
        await manager.broadcast(f"Client #{client_id} joins the chat")
        while True:
            data = await websocket.receive_text()
            await manager.send_personal_data(f"You wrote: {data}")
            await manager.broadcast(f"Client #{client_id} says: {data}")
    except WebSocketDisconnect:
        await manager.broadcast(f"Client #{client_id} left the chat")
        await manager.disconnect(client_id)
